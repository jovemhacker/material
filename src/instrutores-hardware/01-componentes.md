---
track: Hardware
title: Componentes do hardware
---

## Exemplos

Não aplicável.

## Computação desplugada

Não aplicável.

## Comentários

Mostrar que qualquer dispositivo eletrônico possui

-   processador,
-   memória,
-   dispositivo de armazenamento,
-   dispositivo de entrada, e
-   dispositivo de saída.

## Exercícios adicionais

1.  Escolha de dispositivo para comprar

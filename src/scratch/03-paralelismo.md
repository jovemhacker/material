---
track: Scratch
title: Concorrência e Paralelismo
---

Até agora você apenas aninou um único objeto em seus programas.
Nessa seção você vai aprender como animar um segundo objeto.

Quando temos dois ou mais objetos animados ao mesmo tempo, 
dizemos que eles são concorrentes. A concorrência acontece quando
 o computador alterna a execução entre dois ou mais programas, 
dando a impressão de que eles estão executando ao mesmo tempo.

Outro conceito importante em computação é o paralelismo, 
que acontece quando dois ou mais programas estão ``de fato'' 
executando ao mesmo tempo. Para o paralelismo acontecer, 
é preciso ter mais de um processador ou mais de um núcleos de 
processamento dentro do processador (como é o caso dos processadores
dual core, que possuem dois núcleos de processamento).

Nos computadores com apenas um núcleo de processamento, os programas
não executam em paralelo. Os computadores 
mudam muito rapidamente de tarefas (ou programas) de forma que o 
usuário possui a ilusão de que os programas estão sendo executados 
em paralelo, mas na verdade eles são concorrentes.

O primeiro passo para poder animar um segundo objeto é adicioná-lo 
no palco. Assim, teremos objetos concorrentes. 
Você pode fazer isso utilizando

-   `pintar novo objeto`,
-   `escolher um sprinte do arquivo` ou
-   `pegar objeto surpresa`.

Essas opções encontram-se na região destacada abaixo.

![Screenshot destacando os botões para adicionar novo objeto.](/img/scratch/adiciona-objeto.jpg)

Neste exemplo você deve adicionar o objeto `animals/dog1-a`.

![Screenshot do `objecto2` adicionado.](/img/scratch/paralelismo-adiciona.jpg)

Adicione os blocos na aba `Comandos` do `objeto2`
e edite os valores presentes neles para ficar com

~~~
quando ⚑ clicado
vá para x: (150) y: (-100)
aponte para a direção (-90)
deslize em (5) segundos para x: (-150) y: (-100)
~~~

![Screenshot do `objeto2` com a aba "Comandos" preenchida.](/img/scratch/paralelismo-cachorro.jpg)

Depois de programar o `objeto2`
você deve selecionar o `objeto1`, isso é, clicar no `objeto1`,
e programá-lo.

Adicione os blocos na aba `Comandos` do `objeto1`
e edite os valores presentes neles para ficar com

~~~
quando ⚑ clicado
vá para x: (-150) y: (100)
aponte para a direção (90)
deslize en (5) segundos para x: (150) y: (100)
~~~

![Screenshot do `objeto1` com a aba "Comandos" preenchida.](/img/scratch/paralelismo-gato.jpg)

Agora que você programou os dois objetos
você pode executar seu programa
e ver ambos os objetos sendo animados simultaneamente.

![Screenshot do palco ao final da execução.](/img/scratch/paralelismo-bug.jpg)

Ao final da execução do seu programa,
o `objeto2` deve estar de ponta cabeça.
Para corrigir isso, você deve utilizar a opção `somente esquerda-direita`
que encontram-se destacada na figura abaixo.

![Screenshot do palco ao final da execução.](/img/scratch/paralelismo-solucao.jpg)

<section class="exercises">
#### Exercícios

#.  Faça a seguinte animação combinando os comandos abaixo 
(pode usar o mesmo comando mais de uma vez).

![Cenário e comandos para o exercício 1.](/img/scratch/exercicios/fantasma.jpg)

#.  Crie um programa no qual ocorre um diálogo entre dois objetos.
</section>

---
track: Scratch
title: Interface
---

O Scratch é,
de acordo com a [Wikipédia](https://pt.wikipedia.org/wiki/Scratch),
"uma linguagem de programação criada em 2003 pelo Media Lab do MIT". 
O Scratch possui uma interface gráﬁca que oferece uma maneira facilitada
de criar histórias interativas, jogos e animações, além de possibilitar
a importação e criação de vários tipos de mídia (imagens, sons, músicas). 
Os programas são feitos arrastando-se blocos de comandos que devem ser 
encaixados uns nos outros como peças de lego. 

Ao abrir o Scratch você vai se deparar com uma janela semelhante à
ilustrada na figura abaixo.

![Screenshot da tela inicial do Scratch, mostrando a interface da versão 1.4[^1].](/img/scratch/home.jpg)

A interface do Scratch é dividida,
resumidamente,
em quatro áreas:

-   palheta de blocos --- é onde você encontra os blocos que pode utilizar para
    compor seu programa;
-   palco --- é onde você visualiza seu programa sendo executado;
-   lista de objetos --- é onde você encontra os objetos presentes no seu
    programa **e adiciona novos** caso precise;
-   editor de objeto --- é onde você editar os objetos,
    isso é, adiciona ações, trajes, etc. 

Cada uma dessas quatro áreas encontram-se destacadas nas figuras abaixo.

![Screenshot da tela inicial do Scratch destacando a palheta de blocos.](/img/scratch/interface-palheta.jpg)

![Screenshot da tela inicial do Scratch destacando o palco.](/img/scratch/interface-palco.jpg)

![Screenshot da tela inicial do Scratch destacando a lista de objetos.](/img/scratch/interface-objetos.jpg)

![Screenshot da tela inicial do Scratch destacando o editor de objeto.](/img/scratch/interface-editor.jpg)

## Testando blocos

Você pode testar qualquer bloco presente na palheta de blocos
executando um clique duplo no bloco desejado.
Por exemplo,
selecione o palheta `Aparência`
e execute um clique duplo no bloco `diga ()`.

![Screenshot do Scratch ao testar o bloco `diga ()`.](/img/scratch/interface-teste.jpg)

## Palco

#### Criando um palco

Palcos são imagens JPG ou PNG com 480x360 pixels.
Você pode criar novos palcos utilizando qualquer ferramenta
que crie um arquivo JPG ou PNG com 480x360 pixels
como por exemplo
o [Inkscape](https://inkscape.org/en/) ou
o [Gimp](http://www.gimp.org/).

![Palco criado no Inkscape. Os holofotes foram criados por grantm e disponibilizados em https://openclipart.org/detail/12227/student-spotlight sob domínio público.](/img/palco-jovemhacker.svg)

### Importando um palco

O Scratch 1.4 oferece vários palcos prontos para serem usados. 
Para selecionar um palco (seja pronto ou seja criado por você), 
clique no palco em branco e, em seguida, vá na aba `Fundos de Tela`, 
opção importar (veja na figura a seguir). Agora basta navegar pelas
pastas e escolher o palco de sua preferência. 

![Screenshot com destaque para opções de importação de palco e exemplo do palco com o logotipo do Jovem Hacker importado.](/img/scratch/seleciona-palco.jpg)

## Objetos

#### Criando um objeto

Objetos são imagens PNG com fundo transparente[^2].
Você pode criar novos objetos utilizando qualquer ferramenta
que crie um arquivo PNG com fundo transparente
como por exemplo
o [Inkscape](https://inkscape.org/en/) ou
o [Gimp](http://www.gimp.org/). Para usar o palco criado, 
basta selecioná-lo pela aba `Fundos de Tela`, conforme ilustrado na figura anterior.

![Objeto criado no Inkscape.](/img/token-jovemhacker.svg)

Você pode ainda modificar um objeto existente ou criar um objeto utilizando o editor de
pintura do Scratch, conforme mostra a figura a seguir.

![Screenshot do editor de pintura do Scratch.](/img/scratch/editor-pintura.jpg)

### Importando objetos e trajes

O Scratch 1.4 oferece vários objetos (ou sprites) prontos para serem usados. 
Cada objeto pode ter vários trajes. Os trajes são diferentes formas para um
mesmo objeto. Eles são usados para dar movimento ao objeto. 

Para selecionar um objeto (seja pronto ou criado por você), 
clique no botão do meio do menu `Novo sprite` e escolha o objeto de sua 
preferência. Para selecionar um traje, clique no objeto ao qual o traje
será aplicado e vá na aba `Trajes`. A figura a seguir mostra dois trajes
para o objeto morcego, juntamente com as opções para importação
de objetos e trajes.

![Screenshot mostrando dois trajes para o objeto morcego, com destaque para opções de importação de objetos e trajes. ](/img/scratch/objeto-trajes.jpg)

[^1]:   Neste material, todos os screenshots do Scratch foram feitos com a versão 1.4.
[^2]:   Você precisa do fundo transparente caso contrário seu objeto será um
        retângulo. **Imagens JPG não suportam fundo transparente.**

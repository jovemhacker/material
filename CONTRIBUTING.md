Jovem Hacker é um projeto livre
e recebemos contribuições de todas as formas:
novas lições,
correções no material existente,
notificação de erro
e revisão em propostas de alterações.

## Licenciamento

Ao contribuir,
você licencia seu trabalho sobre a licença CC-BY-SA,
a mesma utilizada pelo Jovem Hacker.

## Regras gerais

1.  Caso você goste de lista de emails,
    utilize <http://lists.libreplanetbr.org/mailman/listinfo/jovemhacker>.

1.  O gerenciamento de contribuições
    ocorre em <http://gitlab.com/jovemhacker/material/>.

1.  Para reportar um problema
    ou sugerir uma melhoria
    utilize <https://gitlab.com/jovemhacker/material/issues/new>.

1.  Caso você deseje enviar uma correção ou melhoria
    que você já realizou,
    sugerimos que você fork <http://gitlab.com/jovemhacker/material/>
    e envie um "merge request".

1.  Se você está procurando formas para contribuir
    verifique <https://gitlab.com/jovemhacker/material/issues>.

## Arquivos e diretórios:

-   `CONTRIBUTING.md`

    Armazena as sugestões de contribuição.

-   `dist/`

    É a raiz para a versão compilada em HTML das lições.

-   `gruntfile.js`

    Armazena as regras de compilação das lições para HTML.

-   `LICENSE.md`

    A licença utilizada para as lições. CC-BY-SA.

-   `package.json`

    As dependências do projeto.

-   `README.md`

    Arquivo com explicação breve sobre o projeto.

-   `src/`

    É a raiz para as lições.

-   `src/index.md`

    É a página inicial das lições.
    Contem links para todas as lições.

-   `src/instrutores.md`

    É a página inicial do material para instrutores.
    Contem links para todas as notas de lições.

-   `src/{apresentacao,hardware,python,scratch,web}/[0-9][0-9]-*.md`

    São os arquivos Markdown que apresentam o material.

-   `src/instrutores-{apresentacao,hardware,python,scratch,web}/[0-9][0-9]-*.md`

    São os arquivos Markdown que apresentam comentários sobre o material.

-   `src/instrutores-{apresentacao,hardware,python,scratch,web}/exemplos/`

    É a raiz para os exemplos.

-   `src/css/`

    Armazena as folhas de estilo utilizadas nas lições.

-   `src/img/`

    Armazena imagens utilizadas globalmente.

-   `src/img/{apresentacao,hardware,python,scratch,web}/`

    Armazena imagens utilizadas no material.

-   `templates/`

    Armazena os templates utilizados pelo Pandoc.

## Regras de escrita

1.  Utilizamos a terceira pessoa do singular.
    E.g.

    > Selecione o bloco ...

    **ao invés de**

    > Selecionamos o bloco ...

1.  Utilizamos elementos `<code>`
    para indicar elementos da palheta de blocos do Scratch.
    E.g.

    > Na palheta `Movimento` você encontra ...

1.  Utilizamos parênteses, ( e ), para delimitar
    valores customizáveis nos blocos do Scratch.
    Se estiver indicando qual o bloco
    não utilize nenhum valor entre os parênteses.
    Se estiver representando a aba `Comandos`
    preencha o valor entre parênteses.
    E.g.

    > Utilize o bloco `vá para x: () y: ()`

    e

    > Seu código deve ficar como
    >
    > ~~~
    > quando ⚑ clicado
    > vá para x: (0) y: (0)
    > ~~~

## Guia de estilo para Markdown

Utilizamos [Pandoc](http://pandoc.org/) como ferramenta de conversão
de Markdown para HTML (e futuramente EPUB e PDF).
Pedimos que siga as seguintes regras.

1.  Todo arquivo deve começar com um bloco YAML
    contendo os metadados correspodentes.

1.  Linhas não devem ter mais de 80 caracteres
    **exceto** quando for um link externo.

1.  Títulos de (sub)seções devem utilizar o padrão Atx.
    E.g.

    ~~~
    ## Título da seção
    ~~~

1.  Blocos de código devem ser ser delimitados por `~~~`
    e ter identificado a linguagem do código em questão.
    E.g. um código em Python deve ser escrito como

        ~~~{.python}
        print("Olá")
        ~~~

1.  Listas não ordenadas devem utilizar `-`
    e listas ordenadas devem utilizar `1.`

1.  Tabelas devem ser construidas utilizando `grid_tables`.

1.  É permitido utilizar

    -   `<section class="exercises">`,
        para lista de exercícios, e
    -   `<aside>`,
        para material adicional.

## Guia de estilo para screenshot

Ao fazer uso de um screenshot
ele deve seguir as seguintes regras:

1.  O screenshot shot deve ser reprodutível.
    e os arquivos necessários para isso devem estar armazenados em
    `src/instrutores-*/exemplos/`.

1.  O programa esteja em português (Brasil).

1.  Armazenados em `src/img`.

1.  Utilizado o formato JPG.

1.  Nomeado utilizando apenas letras minúsculas e `-`.

1.  A resolução da tela deve ser 800x600
    e a imagem gerada também deve ter 800x600 pixels.

1.  A janela desejada deve estar no modo "full-screen".

1.  Para destacar algo,
    selecionar uma região retangular
    e contornar a região com uma linha de 20px na cor `#82ed00`.

1.  Para aumentar algo,
    selecionar uma região retangular,
    contornar a região com uma linha de 14px na cor `#ffffff`,
    ampliar a região em 140%
    e ajustar a posição.

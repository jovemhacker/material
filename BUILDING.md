O material está escrito em Markdown
e você precisa convertê-lo para HTML
se desejar utilizá-lo através de um navegador web.

Aqui você vai encontrar instruções
de como converter o material.

## Configuração

### GNU/Linux

Iremos tomar como base a distribuição de GNU/Linux Debian.
Se você utilizar outra distribuição de GNU/Linux
acreditamos que você saberá adaptar as instruções corretamente.

**Como super-usuário:**

1.  Instale [Pandoc]:

        # apt-get install pandoc

1.  Instale [Node]:

        # apt-get install nodejs npm

1.  Instalar o [Grunt]:

        # npm install grunt-cli -g

Como usuário normal:

1.  Acesse o diretório raiz desse projeto.

1.  Instale as dependências locais:

        $ npm install

### Mac OS X/Windows

1.  Instale [Pandoc], [Node] e [Grunt]
    utilizando o instalador disponibilizado
    no respectivos sites.

    **Você irá precissar de acesso administrativo.**

1.  Acesse o diretório raiz desse projeto.

1.  Instale as dependências locais:

        $ npm install

## Conversão

Utilize

    $ grunt serve

O material estará acessível em [aqui][localhost].

Se tiver problemas,
tente

    $ grunt build
    $ grunt serve

e volte a acessar o [endereço anterior][localhost].

[Grunt]: (http://gruntjs.com/)
[Node]: http://npmjs.org/
[Pandoc]: http://pandoc.org/
[localhost]: http://localhost:8100
